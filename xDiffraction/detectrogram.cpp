#include"detectrogram.h"
#include"hdf5_hl.h"
#include"main.h"
#include<QtWidgets>
Detectrogram::Detectrogram(QWidget*parent):QFrame(parent){
    setMinimumHeight(500);
    setMinimumWidth(500);

    QVBoxLayout*layout=new QVBoxLayout(this);
    layout->addWidget(plot);
    plot->setInteractions(QCP::iRangeDrag|QCP::iRangeZoom);
    plot->addPlottable(map);
    QCPColorScale*scale=new QCPColorScale(plot);
    plot->plotLayout()->addElement(0,1,scale);
    map->setColorScale(scale);
    map->setInterpolate(false);
    map->setGradient(QCPColorGradient::gpGrayscale);

    QHBoxLayout*controls=new QHBoxLayout();
    layout->addLayout(controls);

    QSlider*slider=new QSlider(Qt::Horizontal,this);
    QSpinBox*spin=new QSpinBox(this);
    QComboBox*combo=new QComboBox(this);
    QCheckBox*log=new QCheckBox("log",this);
    QCheckBox*lock=new QCheckBox("global scale",this);

    controls->addWidget(slider);
    controls->addWidget(spin);
    controls->addWidget(combo);
    controls->addWidget(log);

    connect(slider,QSlider::valueChanged,this,Detectrogram::load);
    connect(spin,SIGNAL(valueChanged(int)),this,SLOT(load(int)));
    connect(combo,SIGNAL(currentIndexChanged(int)),this,SLOT(setGradient(int)));
    connect(log,QCheckBox::toggled,this,Detectrogram::setScale);
    connect(lock,QCheckBox::toggled,this,Detectrogram::setLock);

    connect(this,Detectrogram::loaded,slider,QSlider::setValue);
    connect(this,Detectrogram::loaded,spin,QSpinBox::setValue);
    connect(this,Detectrogram::max,slider,QSlider::setMaximum);
    connect(this,Detectrogram::max,spin,QSpinBox::setMaximum);

    connect(plot->xAxis,SIGNAL(rangeChanged(QCPRange,QCPRange)),this,SLOT(xRangeChanged(QCPRange,QCPRange)));
    connect(plot->yAxis,SIGNAL(rangeChanged(QCPRange,QCPRange)),this,SLOT(yRangeChanged(QCPRange,QCPRange)));
    connect(plot,QCustomPlot::mousePress,[=](QMouseEvent*event){
        if(event->button()==Qt::RightButton){
            plot->rescaleAxes();
            plot->replot();
        }
    });
    slider->setMaximum(0);
    spin->setMaximum(0);
    log->toggle();
    lock->toggle();

    combo->addItem("Grayscale");
    combo->addItem("Hot");
    combo->addItem("Cold");
    combo->addItem("Night");
    combo->addItem("Candy");
    combo->addItem("Geography");
    combo->addItem("Ion");
    combo->addItem("Thermal");
    combo->addItem("Polar");
    combo->addItem("Spectrum");
    combo->addItem("Jet");
    combo->addItem("Hues");
}
bool Detectrogram::load(int index){
    bool result;
    hid_t group;
    char name[24];
    sprintf(name,"det%d",index);
#pragma omp critical
    {
        group=H5Gopen1(hdf5file,"Detector");
        result=H5LTfind_dataset(group,name);
    }
    if(result){
        hsize_t dims[2];
        int images;
#pragma omp critical
        {
            H5LTget_attribute_int(group,".","Images Completed",&images);
            H5LTget_dataset_info(group,name,dims,0,0);
        }
        double dataset[dims[0]][dims[1]];//dynamically sized arrays aren't actually C++ (they're C99), but are very convenient here
#pragma omp critical
        H5LTread_dataset_double(group,name,*dataset);
        QCPColorMapData*data=map->data();
        data->setSize(dims[0],dims[1]);
        data->setRange(QCPRange(0.5,dims[0]-.5),QCPRange(0.5,dims[1]-.5));
        for(unsigned int x=0;x<dims[0];++x)for(unsigned int y=0;y<dims[1];++y)data->setCell(x,y,dataset[x][y]);
        setLock(locked);
        emit loaded(index);
        emit max(images-1);
    }
#pragma omp critical
    H5Gclose(group);
    return result;
}
void Detectrogram::setGradient(int gradient){
    map->setGradient((QCPColorGradient::GradientPreset)gradient);
    plot->replot();
}
void Detectrogram::setScale(bool scale){
    map->setDataScaleType((QCPAxis::ScaleType)scale);
    plot->replot();
}
void Detectrogram::setLock(bool lock){
    if(locked=lock){
        QCPRange range=map->dataRange();
#pragma omp critical
        if(hdf5file>0){
            hid_t group=H5Gopen1(hdf5file,"/Detector");
            if(H5LTfind_attribute(group,"Lowest Value"))H5LTget_attribute_double(group,".","Lowest Value",&range.lower);
            if(H5LTfind_attribute(group,"Highest Value"))H5LTget_attribute_double(group,".","Highest Value",&range.upper);
            H5Gclose(group);
        }
        map->setDataRange(range);
    }else map->rescaleDataRange(true);
    plot->replot();
}
void Detectrogram::xRangeChanged(const QCPRange&attempted,const QCPRange&old){
    if(attempted.size()<6)plot->xAxis->setRange(old.size()<6?QCPRange(old.lower-3,old.upper+3):old);
    /*const QCPRange allowed=map->data()->keyRange();
    if(attempted.lower<allowed.lower||attempted.upper>allowed.upper){
        if(old.lower<allowed.lower||old.upper>allowed.upper)plot->xAxis->setRange(allowed);
        else plot->xAxis->setRange(old);
    }*/
}
void Detectrogram::yRangeChanged(const QCPRange&attempted,const QCPRange&old){
    if(attempted.size()<6)plot->yAxis->setRange(old.size()<6?QCPRange(old.lower-3,old.upper+3):old);
    /*const QCPRange allowed=map->data()->valueRange();
    if(attempted.lower<allowed.lower||attempted.upper>allowed.upper){
        if(old.lower<allowed.lower||old.upper>allowed.upper)plot->yAxis->setRange(allowed);
        else plot->yAxis->setRange(old);
    }*/
}
