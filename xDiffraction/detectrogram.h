#ifndef DETECTROGRAM_H
#define DETECTROGRAM_H
#include"qcustomplot.h"
#include"hdf5.h"
class Detectrogram:public QFrame{
    Q_OBJECT
    bool locked=false;
    QCustomPlot*plot=new QCustomPlot(this);
    QCPColorMap*map=new QCPColorMap(plot->xAxis,plot->yAxis);
public:
    Detectrogram(QWidget*parent=0);
signals:
    void loaded(int index=0);
    void max(int maximum=0);
public slots:
    bool load(int index=0);
    void setGradient(int gradient=0);
    void setScale(bool scale=0);
    void setLock(bool lock=0);
private slots:
    void xRangeChanged(const QCPRange&range,const QCPRange&old);
    void yRangeChanged(const QCPRange&range,const QCPRange&old);
};
#endif
