#ifndef DIRECTIONCALCULATOR_H
#define DIRECTIONCALCULATOR_H
#include"dialog.h"
#include"param.h"
#include"main.h"
class DirectionCalculator:public Dialog{
    DoubleParam*spacingParam,*energyParam,*braggParam;
    VectorParam*hParam,*beamParam;
    QVBoxLayout*vertical=new QVBoxLayout(frame);
#define _ new QLineEdit(this)
    QLineEdit*spacingBox=_,*energyBox=_,*braggBox=_,*hx=_,*hy=_,*hz=_,*userx=_,*usery=_,*userz=_,*resx=_,*resy=_,*resz=_;
#undef _
public:
    DirectionCalculator(QWidget*parent=0);
    vector result();
    void setParams(DoubleParam*,DoubleParam*,DoubleParam*,VectorParam*,VectorParam*);
    void setVisible(bool);
private slots:
    void calculate();
    void onAccept();
};
#endif
