/*Numerical simulation code for x-ray dynamical diffraction from a particle,*/
/*based on the iterative solving algorithm published in PRB 89, 014104 (2014)*/
/*Written by Hanfei Yan, 2014*/

///OpenMP & CUDA functionality added by Ryan Hilbert
///Added comments preceded by triple slash, "///"

//V4.0

///Name of the output directory; make sure this exists!
const char dir_name[] = "output4";

#undef __STRICT_ANSI__ ///allows non-standard j0 bessel function to be defined in MinGW's math.h

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

#ifdef _OPENMP
#include<omp.h>
#endif

#ifdef __CUDACC__
#include<cuComplex.h>
#endif

#include"main.h"

hid_t hdf5file=0;

//parameter passed from GUI
/*
const double cube_l = 0.15;
const double cube_w = 0.15;
const double cube_h = 0.15;
const double sphere_r = 0.3;
*/
const double amplitude = 400.0; //flux = amplitude^2*area

const double spiral_phi = 2.4; //radian

//end of parameter initilization

#ifdef __CUDACC__
__device__ const cuDoubleComplex i = { 0, 1 };
__device__ const cuDoubleComplex wave = { 0, wave_number };

__device__ cuDoubleComplex cuCexp(cuDoubleComplex num){
    cuDoubleComplex res;
    const double t = exp(num.x);
    sincos(num.y, &res.y, &res.x);
    res.x *= t;
    res.y *= t;
    return res;
}
__global__ void kernel(vector* input, cuDoubleComplex* output, cuDoubleComplex factor, vector avg){
    const int id = blockDim.x*blockIdx.x + threadIdx.x;
    vector in = input[id];
    const double distance = sqrt((in.x - avg.x)*(in.x - avg.x) + (in.y - avg.y)*(in.y - avg.y) + (in.z - avg.z)*(in.z - avg.z));
    output[id] = cuCsub(output[id], cuCmul(cuCdiv(i, make_cuDoubleComplex(lambda*distance, 0)), cuCmul(factor, cuCexp(cuCmul(wave, make_cuDoubleComplex(distance, 0))))));
}
#endif

#ifdef __cplusplus
#define FOREACH(name,type,var,...) type var = __VA_ARGS__;
#include"main.h"
#define printf(...) {char string[BUFSIZ];sprintf(string,__VA_ARGS__);emit output(string);}
void Main::begin(){///This wrapper around the start() method is needed to ensure connected slots are triggered before the thread starts executing
    if(!isRunning()){
        emit starting();
        start();
    }
}
#include"window.h"
#include<QApplication>
int main(int argc,char*argv[]){
    QApplication app(argc,argv);
    Window win;
    win.show();
    return app.exec();
}
void Main::stop(){stopped=true;}
void Main::run(){
#else
#include"main.h"
int stopped=0,steps=0;
int main(){
#define FOREACH(name,type,var,...) var = __VA_ARGS__;
#include"main.h"///if compiling as a raw C program, initialize the global parameters here
#endif
    time_t now, end;
    double seconds;
    time(&now);

    ///print the value of each parameter as the computation code sees it (disregard printed units; they do not take the conversion multiplier into account)
#define FOREACH(name,type,var,...) \
    if(sizeof(type)==sizeof(int)){printf("%s = %d",name,*(int*)&var);}\
    else if(sizeof(type)==sizeof(double)){printf("%s = %f",name,*(double*)&var);}\
    else if(sizeof(type)==sizeof(complex)){printf("%s = %f %+fi",name,creal(*(complex*)&var),cimag(*(complex*)&var));}\
    else if(sizeof(type)==sizeof(vector)){printf("%s = %fx %+fy %+fz",name,((vector*)&var)->x,((vector*)&var)->y,((vector*)&var)->z);}
#include"main.h"

    const double lambda = 12.398/g_energy*1e-4;
    const double wave_number = 2.0*pi/lambda;

    //following parameters are passed from GUI
    ///const double d_spacing = (5.43 / 8.0)*1e-4;
    ///const double ds = 0.005, dy = 0.005; //step size in oblique coordinates system (ds) and out of plane direction (dy)

    const vector start_position = g_r0;
    ///const complex  chi_0 = -0.74984e-5 + I*0.88508e-7;
    ///const complex  chi_h = -0.17751e-5 + I*0.66803e-7;

    ///vector unit_h = {0, 0, -1};
    vector unit_s0 = vec_num(1.0/vec_abs(g_unit_s0),g_unit_s0);
    vector unit_s0_0 = unit_s0;///{0.598481, 0.0, 0.801137}; // initial value passed from GUI; does not change during the calculation

    //focus beam parameters
    //gauss beam
    const double beam_width = 0.15;
    //FZP parameters
    const double dia_inner = 20.0, dia_outer = 80.0;///, focal_length = 147000.0;

    //end of parameter initialization

    vector unit_h = vec_num(1.0/vec_abs(g_unit_h),g_unit_h);
    vector h = vec_num(2.0*pi / g_d_spacing, unit_h);
    ///const double th_Bragg = asin(lambda / (2.0*d_spacing));
    printf("Bragg angle = %f degree\n", g_th_Bragg * 180 / pi);

    //define detector coordinats system and rotation axis

    vector det_x;
    det_x = vec_cross(unit_s0_0,unit_h); //detector column direction is perpendicular to the diffraction plane define by s0 and h
    det_x = vec_num(1.0/vec_abs(det_x), det_x); // make it a unit vector

    vector det_z;
    det_z = vec_plus(vec_num(wave_number, unit_s0_0), h); //detector plane normal is along kh direction
    det_z = vec_num(1.0/vec_abs(det_z), det_z); // make a unit vector

    vector det_y = vec_cross(det_x, det_z);

    vector rot_axis = vec_num(-1.0, det_x); // rotation in the diffraction plane and a positive angle make the incidence angle to the diffracting lattice place larger.

    //create a log file
    char log_file_name[FILENAME_MAX];
    strcpy(log_file_name, dir_name);
    strcat(log_file_name, "/log.txt");
    FILE * log;
    log = fopen(log_file_name,"w+");

#ifdef _OPENMP
    const int thread_count = omp_get_num_procs();
    omp_set_dynamic(0);
    printf("OpenMP Enabled: %d Threads\n", thread_count);
#else
    printf("OpenMP not enabled; limited to one GPU maximum!\n");
#endif
#ifdef __CUDACC__
    int device_count = 0;
    cudaGetDeviceCount(&device_count);
    cudaDeviceProp properties;
    cudaGetDeviceProperties(&properties,0);
    const int block_size = properties.maxThreadsPerBlock;
#else
    printf("CUDA not enabled!\n");
#endif
    ////////////////////////////////////////////////////////
    //Note: xyz is crystal coordinates system
    const int num_iterations=g_num_scan*g_num_angle;
    emit max(num_iterations);
#pragma omp critical
    {
        hid_t group=H5Gopen1(hdf5file,"/Detector");
        H5LTset_attribute_int(group,".","Number of Images",&num_iterations,1);
        int temp=0;
        if(H5LTfind_attribute(group,"Images Completed"))H5LTget_attribute_int(group,".","Images Completed",&temp);
        steps=temp;
        H5Gclose(group);
    }
    emit progress(steps);
#pragma omp parallel for schedule(dynamic) num_threads(thread_count)
    for(int file_num = steps; file_num < num_iterations; ++file_num)if(!stopped){///integer counter needed for OpenMP

        const double dth = floor(file_num / g_num_scan)*g_dth_step + g_dth_start;//deviation angle to the exact Bragg angle; scan position first
        const vector beam_center = { g_spiral_c*sqrt(file_num % g_num_scan)*cos(spiral_phi*(file_num % g_num_scan)) + start_position.x, g_spiral_c*sqrt(file_num % g_num_scan)*sin(spiral_phi*(file_num % g_num_scan)) + start_position.y, start_position.z };

        ///fprintf(log, "%d\t%f\t%f\t%f\n", file_num, beam_center.x, beam_center.y, dth*180.0/pi);///Line causes error "Invalid parameter passed to C runtime function."

        //calculate the new k0 after a roration
        vector unit_s0;
        matrix R;
        R = rot_m(rot_axis, dth);
        unit_s0 = rotation(R, unit_s0_0);
        vector k0;
        k0 = vec_num(wave_number, unit_s0);

        //calculate the new kh after a roration
        vector kh;
        kh = vec_plus(k0, h);

        //calculate the corresponding unit vectors
        vector unit_sh;
        unit_sh = vec_num(1.0/vec_abs(kh), kh);
        vector unit_sy;
        unit_sy = vec_cross(unit_s0,unit_sh);
        unit_sy = vec_num(1.0/vec_abs(unit_sy),unit_sy);

        char det_file_name[FILENAME_MAX];
        snprintf(det_file_name,sizeof det_file_name,"%s/det%d.h5",dir_name,file_num);

        ///char dh_file_name[FILENAME_MAX], d0_file_name[FILENAME_MAX], poynting_file_name[FILENAME_MAX], boundary_dh_file_name[FILENAME_MAX], boundary_d0_file_name[FILENAME_MAX];
        ///strcpy(det_file_name, dir_name);
        /*
        strcpy(dh_file_name, dir_name);
        strcpy(d0_file_name, dir_name);
        strcpy(poynting_file_name, dir_name);
        strcpy(boundary_dh_file_name, dir_name);
        strcpy(boundary_d0_file_name, dir_name);

        sprintf(char_n, "%d", file_num);
        strcat(det_file_name, "/det_");
        strcat(det_file_name, char_n);
        strcat(det_file_name, ".txt");

        strcat(dh_file_name, "/dh_");
        strcat(dh_file_name, char_n);
        strcat(dh_file_name, ".txt");
        strcat(d0_file_name, "/d0_");
        strcat(d0_file_name, char_n);
        strcat(d0_file_name, ".txt");
        strcat(poynting_file_name, "/poynting_");
        strcat(poynting_file_name, char_n);
        strcat(poynting_file_name, ".txt");
        strcat(boundary_d0_file_name, "/boundary_d0_");
        strcat(boundary_d0_file_name, char_n);
        strcat(boundary_d0_file_name, ".txt");
        strcat(boundary_dh_file_name, "/boundary_dh_");
        strcat(boundary_dh_file_name, char_n);
        strcat(boundary_dh_file_name, ".txt");
        */



        //create a grid in oblique coordinates system

        //prepare output files
        /*
        FILE * output_dh;
        FILE * output_d0;
        FILE * output_poynting;
        FILE * output_boundary_d0;
        FILE * output_boundary_dh;
        */
        ///FILE * output_det;
        /*
        output_dh = fopen(dh_file_name, "w+");
        output_d0 = fopen(d0_file_name, "w+");
        output_poynting = fopen(poynting_file_name, "w+");
        output_boundary_d0 = fopen(boundary_d0_file_name, "w+");
        output_boundary_dh = fopen(boundary_dh_file_name, "w+");
        */
        ///output_det = fopen(det_file_name, "w+");

        ////////////////////////////////////////////

        //initilize detector parameters
        detector det_para;
        vector pos_temp, pos_avg, pos_dif;
        complex val_temp, val_avg;
        det_para.pix_size = g_pixel_size;
        det_para.row = g_row;
        det_para.column = g_column;
        det_para.dist = g_det_dist;

        complex *det = (complex*)calloc(det_para.row*det_para.column, sizeof(complex));
        vector *det_pix = (vector*)calloc(det_para.row*det_para.column, sizeof(vector)); //position of a pixel on detector
        for (int i = 0; i < det_para.row; i++){
            for (int j = 0; j < det_para.column; j++){
                det_pix[i*det_para.column+j] = vec_plus(vec_num((det_para.column/2 - j)*det_para.pix_size, det_x), vec_num((det_para.row/2 - i)*det_para.pix_size, det_y));
                det_pix[i*det_para.column+j] = vec_plus(det_pix[i*det_para.column+j], vec_num(det_para.dist, det_z));
                det_pix[i*det_para.column+j] = rotation(R, det_pix[i*det_para.column+j]);
            }
        }
        const int det_size = det_para.row*det_para.column;
        int thread_id = 0;
#ifdef _OPENMP
        thread_id = omp_get_thread_num();
#endif
#ifdef __CUDACC__
        const int grid_size = (det_size + block_size - 1)/block_size;
        cudaSetDevice(thread_id%device_count);
        vector*cuda_det_pix;
        cuDoubleComplex*cuda_det;
        cudaMalloc(&cuda_det_pix, det_para.row*det_para.column*sizeof*cuda_det_pix);
        cudaMalloc(&cuda_det, det_para.row*det_para.column*sizeof*cuda_det);
        cudaMemset(cuda_det, 0, det_para.row*det_para.column*sizeof*cuda_det);
        cudaMemcpy(cuda_det_pix, det_pix, det_para.row*det_para.column*sizeof*cuda_det_pix, cudaMemcpyHostToDevice);
#endif
        ///all printing in one statement eliminates concurrency issues
        printf("\nThread %d:\n%s\nBeam_x = %f\nBeam_y = %f\n", thread_id, det_file_name,  beam_center.x, beam_center.y);
        ///////////////////////////////////////////

        //start calculation
        int flag_y_pos = 0, flag_y_neg = 0; //two status flags to show whether the positive or negive sy direction is searched
        int c = 0;
        double y;
        while (flag_y_pos == 0 || flag_y_neg == 0){	//search is not done
            if (flag_y_pos == 0){ //positive direction search is not done
                y = c*g_dy; //step toward positive sy axis
                c++;
            }
            else{
                y = -c*g_dy; //step toward negative sy direction
                c++;
            }
            //find the initial guess of the size of the grid in s0 direction
            int a = 0;
            int b = 0;
            const double ds=g_ds;
            vector oblique;
            oblique.x = b*ds;
            oblique.y = y;
            oblique.z = a*ds;
            vector cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
            while (crystal(cartesian) == 1){
                b++;
                oblique.x = b*ds;
                cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
            }
            int grid_size_s0 = 2 * b;
            //find the initial guess of the size of the grid in sh direction
            a = 0;
            b = 0;
            oblique.x = b*ds;
            oblique.y = y;
            oblique.z = a*ds;
            cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
            while (crystal(cartesian) == 1){
                a++;
                oblique.z = a*ds;
                cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
            }
            int grid_size_sh = 2 * a;

            int status_s0 = 1;
            int status_sh = 1;
            //find the smallest grid that accomodate the particle
            //check four boundary lines of the grid, if there is an interception with the crystal, increase the size
            while (status_s0 == 1 || status_sh == 1){
                status_s0 = 0;
                oblique.x = (grid_size_s0 / 2)*ds;
                oblique.y = y;
                for (int i = -grid_size_sh / 2; i <= grid_size_sh / 2; i++){
                    oblique.z = i*ds;
                    cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    if (crystal(cartesian) == 1){ //too small
                        status_s0 = 1;
                    }
                }
                oblique.x = -(grid_size_s0 / 2)*ds;
                oblique.y = y;
                for (int i = -grid_size_sh / 2; i <= grid_size_sh / 2; i++){
                    oblique.z = i*ds;
                    cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    if (crystal(cartesian) == 1){ //too small
                        status_s0 = 1;
                    }
                }
                if (status_s0 == 1) {// there is an interception in sh direction
                    grid_size_s0 += 2; // increase the size in s0 direction
                }
                status_sh = 0;
                oblique.z = (grid_size_sh / 2)*ds;
                oblique.y = y;
                for (int j = -grid_size_s0 / 2; j <= grid_size_s0 / 2; j++){
                    oblique.x = j*ds;
                    cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    if (crystal(cartesian) == 1){ //too small
                        status_sh = 1;
                    }
                }
                oblique.z = -(grid_size_sh / 2)*ds;
                oblique.y = y;
                for (int j = -grid_size_s0 / 2; j <= grid_size_s0 / 2; j++){
                    oblique.x = j*ds;
                    cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    if (crystal(cartesian) == 1){ //too small
                        status_sh = 1;
                    }
                }
                if (status_sh == 1){// there is an interception
                    grid_size_sh += 2; // increase the size
                }
            }
            //take into account zero

            grid_size_sh++;
            grid_size_s0++;


            if (grid_size_s0 == 1 && grid_size_sh == 1){ // no interception point found
                if (flag_y_pos == 0){ //search on the positive direction
                    flag_y_pos = 1; //postive y_max found
                    c = 1; //reset the counter to step along the negative direction
                }
                else {
                    flag_y_neg = 1; //negative y_min found
                }
            }
            //allocate memory for complex matrix d0 and dh, the wavefields for incident and diffracted waves
            complex *dh = (complex*)malloc(sizeof(complex)*grid_size_s0*grid_size_sh);
            complex *d0 = (complex*)malloc(sizeof(complex)*grid_size_s0*grid_size_sh);
            double *phase = (double*)malloc(sizeof(double)*grid_size_s0*grid_size_sh);
            vector *poynting = (vector*)calloc(grid_size_s0*grid_size_sh, sizeof(vector));
            boundary *bound_d0 = (boundary*)malloc(sizeof(boundary)*grid_size_sh);
            boundary *bound_dh = (boundary*)malloc(sizeof(boundary)*grid_size_s0);

            //initilize the wavefield and find the boundary
            //found boundary conditions
            for (int i = 0; i < grid_size_sh; i++){
                status_s0 = 0;
                oblique.z = -(grid_size_sh - 1)*ds / 2.0 + i*ds;
                oblique.y = y;
                for (int j = 0; j < grid_size_s0; j++){
                    oblique.x = -(grid_size_s0 - 1)*ds / 2.0 + j*ds;
                    cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    if (crystal(cartesian) == 1){
                        if (status_s0 == 0){
                            bound_d0[i].back = j;
                            status_s0++;
                        }
                        else if (status_s0 == 1){
                            bound_d0[i].front = j;
                            status_s0++;
                        }
                        else {
                            bound_d0[i].front = j;
                        }
                    }
                }
                bound_d0[i].num = status_s0;
                if (status_s0 == 1){
                    bound_d0[i].front = bound_d0[i].back;
                }
                //      printf("back = %d    front = %d\n", bound_d0[i].back, bound_d0[i].front);
            }
            for (int j = 0; j < grid_size_s0; j++){
                status_sh = 0;
                oblique.x = -(grid_size_s0 - 1)*ds / 2.0 + j*ds;
                oblique.y = y;
                for (int i = 0; i < grid_size_sh; i++){
                    oblique.z = -(grid_size_sh - 1)*ds / 2.0 + i*ds;
                    cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    if (crystal(cartesian) == 1){
                        if (status_sh == 0){
                            bound_dh[j].back = i;
                            status_sh++;
                        }
                        else if (status_sh == 1){
                            bound_dh[j].front = i;
                            status_sh++;
                        }
                        else {
                            bound_dh[j].front = i;
                        }
                    }
                }
                bound_dh[j].num = status_sh;
                if (status_sh == 1){
                    bound_dh[j].front = bound_dh[j].back;
                }
            }
            //  printf("boundary is defined\n");
            //initilize dh field and the phase due to a displacement
            for (int i = 0; i < grid_size_sh; i++){
                for (int j = 0; j < grid_size_s0; j++){
                    dh[i*grid_size_s0 + j] = 0.0;
                    oblique.x = -(grid_size_s0 - 1)*ds / 2.0 + j*ds;
                    oblique.z = -(grid_size_sh - 1)*ds / 2.0 + i*ds;
                    oblique.y = y;
                    cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    phase[i*grid_size_s0 + j] = vec_dot(h, displacement(cartesian));
                }
            }
            //  printf("dh and phase initilization is done\n");
            //initilize d0 field, calculated from the beam function
            for (int i = 0; i < grid_size_sh; i++){
                if (bound_d0[i].num == 0){
                    for (int j = 0; j < grid_size_s0; j++){
                        d0[i*grid_size_s0 + j] = 0.0; //outside the particle
                    }
                }
                else {
                    for (int j = 0; j < grid_size_s0; j++){
                        if (j >= bound_d0[i].back && j <= bound_d0[i].front){
                            oblique.x = -(grid_size_s0 - 1)*ds / 2.0 + j*ds;
                            oblique.z = -(grid_size_sh - 1)*ds / 2.0 + i*ds;
                            oblique.y = y;
                            cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                            switch (g_beam_type){
                            case 0:
                                d0[i*grid_size_s0 + j] = planewave_beam(cartesian);
                                break;
                            case 1:
                                d0[i*grid_size_s0 + j] = gauss_beam(wave_number, beam_width, unit_s0, beam_center, cartesian);
                                break;
                            case 2:
                                d0[i*grid_size_s0 + j] = FZP(wave_number, dia_inner, dia_outer, g_focal_length, unit_s0, beam_center, cartesian);
                                break;
                            }
                        }
                        else {
                            d0[i*grid_size_s0 + j] = 0.0; //outside the particle
                        }
                    }
                }
            }

            //  printf("Initilization finished\n");
            //iteratively updating both fields

            complex *integral_s0 = (complex*)calloc(grid_size_s0, sizeof(complex));
            complex *integral_sh = (complex*)calloc(grid_size_sh, sizeof(complex));
            complex *dh_tmp = (complex*)calloc(grid_size_s0, sizeof(complex));
            complex ch = I*wave_number / 2.0*chi_h;
            complex c0 = I*wave_number / 2.0*chi_0;
            complex w0 = (1.0 - pow(vec_abs(kh) / wave_number, 2.0)) + chi_0;
            complex cw = I*wave_number*w0 / 2.0;
            complex cp, gp;
            double s0, sh, res = 1.0, avg = 0.0;
            int k = 0;

            //  printf("start iteration\n");
            while (res > g_tolerance){
                k++;
                res = 0.0;
                avg = 0.0;
                // updating d0
                for (int i = 0; i < grid_size_sh; ++i){
                    if (bound_d0[i].num > 1){
                        integral_s0[bound_d0[i].back] = 0.0;
                        for (int j = bound_d0[i].back + 1; j <= bound_d0[i].front; ++j){
                            s0 = -(grid_size_s0 - 1)*ds*0.5 + (j - 1)*ds;
                            integral_s0[j] = integral_s0[j - 1] + ch*(dh[i*grid_size_s0 + j] + dh[i*grid_size_s0 + j - 1])*0.5*cexp(-c0*s0)*(1.0 - cexp(-c0*ds)) / c0;
                            s0 += ds;
                            d0[i*grid_size_s0 + j] = d0[i*grid_size_s0 + bound_d0[i].back] * cexp((j - bound_d0[i].back)*ds*c0) + cexp(c0*s0)*integral_s0[j];
                        }
                    }
                }
                //updating dh
                for (int j = 0; j < grid_size_s0; ++j){
                    if (bound_dh[j].num > 1){
                        integral_sh[bound_dh[j].back] = 0.0;
                        for (int i = bound_dh[j].back + 1; i <= bound_dh[j].front; ++i){
                            sh = -(grid_size_sh - 1)*ds*0.5 + (i - 1)*ds;
                            cp = I*phase[(i - 1)*grid_size_s0 + j];
                            gp = I*(phase[i*grid_size_s0 + j] - phase[(i - 1)*grid_size_s0 + j]) / ds;
                            integral_sh[i] = integral_sh[i - 1] + (d0[i*grid_size_s0 + j] + d0[(i - 1)*grid_size_s0 + j])*0.5*cexp(-cp)*(1.0 - cexp(-(cw + gp)*ds))*cexp(-cw*sh) / (cw + gp);
                            sh += ds;
                            dh[i*grid_size_s0 + j] = ch*cexp(I*phase[i*grid_size_s0 + j] + cw*sh)*integral_sh[i];
                        }
                        res = res + cabs(cpow(dh_tmp[j] - dh[bound_dh[j].front*grid_size_s0 + j], 2.0));
                        avg = avg + cabs(cpow(dh[bound_dh[j].front*grid_size_s0 + j], 2.0));
                        dh_tmp[j] = dh[bound_dh[j].front*grid_size_s0 + j];
                    }
                }
                if (avg == 0.0){
                    res = 0.0;
                }
                else {
                    res = sqrt(res/avg);
                }
            }
            for (int i = 0; i < grid_size_sh; i++){
                if (bound_d0[i].num > 0){
                    for (int j = bound_d0[i].back; j <= bound_d0[i].front; j++){
                        poynting[i*grid_size_s0 + j] = vec_plus(vec_num(pow(cabs(dh[i*grid_size_s0 + j]), 2.0), kh), vec_num(pow(cabs(d0[i*grid_size_s0 + j]), 2.0), k0));
                    }
                }
            }
            /*
            for (int i = 0; i < grid_size_sh; i++){
                if (bound_d0[i].num > 0){
                    for (int j = bound_d0[i].back; j <= bound_d0[i].front; j++){
                        oblique.x = -(grid_size_s0 - 1)*ds / 2.0 + j*ds;
                        oblique.z = -(grid_size_sh - 1)*ds / 2.0 + i*ds;
                        oblique.y = y;
                        cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);

                        //fprintf(output_dh, "%f      %f      %f      %f\n", cartesian.x, cartesian.y, cartesian.z, pow(cabs(dh[i*grid_size_s0 + j]), 2.0));
                        //fprintf(output_d0, "%f      %f      %f      %f\n", cartesian.x, cartesian.y, cartesian.z, pow(cabs(d0[i*grid_size_s0 + j]), 2.0));
                        //fprintf(output_poynting, "%f      %f      %f      %f      %f\n", cartesian.x, cartesian.y, cartesian.z, poynting[i*grid_size_s0 + j].x, poynting[i*grid_size_s0 + j].z);
                    }
                }
            }
            for (int i = 0; i < grid_size_sh; i++){
                if (bound_d0[i].num > 0){
                    oblique.x = -(grid_size_s0 - 1)*ds / 2.0 + bound_d0[i].front*ds;
                    oblique.z = -(grid_size_sh - 1)*ds / 2.0 + i*ds;
                    oblique.y = y;
                        cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    //fprintf(output_boundary_d0, "%f      %f      %f      %e\n", cartesian.x, cartesian.y, cartesian.z, pow(cabs(d0[i*grid_size_s0 + bound_d0[i].front]), 2.0));
                }
            }
            for (int j = 0; j < grid_size_s0; j++){
                if (bound_dh[j].num > 0){
                    oblique.x = -(grid_size_s0 - 1)*ds / 2.0 + j*ds;
                    oblique.z = -(grid_size_sh - 1)*ds / 2.0 + bound_dh[j].front*ds;
                    oblique.y = y;
                    sh = (bound_dh[j].front - bound_dh[j].back)*ds;
                    cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    //fprintf(output_boundary_dh, "%f      %f      %f      %e\n", cartesian.x, cartesian.y, cartesian.z, pow(cabs(dh[bound_dh[j].front*grid_size_s0 + j]), 2.0));
                }
            }
            */
            //propagate to farfield detector

            int flag = 0;
            for (int k = 0; k < grid_size_s0; k++){
                if (bound_dh[k].num > 0){
                    oblique.x = -(grid_size_s0 - 1)*ds*0.5 + k*ds;
                    oblique.z = -(grid_size_sh - 1)*ds*0.5 + bound_dh[k].front*ds;
                    oblique.y = y;
                    cartesian = oblique_to_cartesian(oblique.x, unit_s0, oblique.y, unit_sy, oblique.z, unit_sh);
                    if (flag == 1){ //boundary points more than two
                        pos_avg = vec_num(0.5, vec_plus(pos_temp, cartesian));
                        pos_dif = vec_minus(cartesian, pos_temp);
                        val_avg = (val_temp + dh[bound_dh[k].front*grid_size_s0 + k])*0.5;
                        complex factor = g_dy*csqrt(cpow(vec_abs(pos_dif), 2) - cpow(vec_dot(pos_dif, kh), 2) / cpow(vec_abs(kh), 2))*cexp(I*vec_dot(kh, pos_avg) - I*vec_dot(h, displacement(pos_avg)))*val_avg;
#ifdef __CUDACC__
                        kernel<<<grid_size, block_size>>>(cuda_det_pix, cuda_det, make_cuDoubleComplex(creal(factor), cimag(factor)), pos_avg);
#else
                        for (int i = 0; i < det_size; i++){
                            double distance = vec_dist(det_pix[i], pos_avg);
                            det[i] -= (I / (lambda*distance))*factor*cexp(I*distance*wave_number);
                        }
#endif
                        val_temp = dh[bound_dh[k].front*grid_size_s0 + k];
                        pos_temp.x = cartesian.x;
                        pos_temp.y = cartesian.y;
                        pos_temp.z = cartesian.z;
                    }
                    else {
                        val_temp = dh[bound_dh[k].front*grid_size_s0 + k];
                        pos_temp.x = cartesian.x;
                        pos_temp.y = cartesian.y;
                        pos_temp.z = cartesian.z;
                        flag = 1;
                    }
                }
            }
            free(dh);
            free(d0);
            free(phase);
            free(poynting);
            free(bound_d0);
            free(bound_dh);
            free(integral_s0);
            free(integral_sh);
        }
#ifdef __CUDACC__///copy results from GPU and free used GPU memory
        cudaMemcpy(det, cuda_det, det_para.row*det_para.column*sizeof*det, cudaMemcpyDeviceToHost);
        cudaFree(cuda_det);
        cudaFree(cuda_det_pix);
#endif
        //write to detector file
        double min=INFINITY,max=-INFINITY;
        double*data = (double*)det;///re-use the det memory block to store results
        for(int i=0; i<det_size; ++i){
            data[i] = det_para.pix_size*det_para.pix_size*pow(cabs(det[i]), 2);
            if(data[i]<min)min=data[i];
            if(data[i]>max)max=data[i];
        }
        const hsize_t dims[] = {det_para.row, det_para.column};
        char name[24];///det(3) + file_num(20 max) + null(1) = 24 character maximum
        sprintf(name,"det%d",file_num);
        double old_min=INFINITY,old_max=-INFINITY;
        const char min_name[]="Lowest Value";
        const char max_name[]="Highest Value";
#pragma omp critical
        {
            hid_t group=H5Gopen1(hdf5file,"/Detector");
            H5LTmake_dataset_double(group, name, 2, dims, data);
            if(H5LTfind_attribute(group,min_name))H5LTget_attribute_double(group,".",min_name,&old_min);
            if(H5LTfind_attribute(group,max_name))H5LTget_attribute_double(group,".",max_name,&old_max);
            if(min<old_min)H5LTset_attribute_double(group,".",min_name,&min,1);
            if(max>old_max)H5LTset_attribute_double(group,".",max_name,&max,1);
            if(steps<=file_num){
                const int temp=steps=file_num+1;
                H5LTset_attribute_int(group,".","Images Completed",&temp,1);
            }
            H5Gclose(group);
        }
        emit progress(steps);
        /*for (int i = 0; i < det_para.row; i++){
            for (int j = 0; j < det_para.column; j++){
                fprintf(output_det, "%e\t", pow(det_para.pix_size,2)*pow(cabs(det[i*det_para.column + j]), 2));
            }
            fprintf(output_det, "\n");
        }*/
        free(det);
        free(det_pix);
        /*
        fclose(output_dh);
        fclose(output_d0);
        fclose(output_poynting);
        fclose(output_boundary_d0);
        fclose(output_boundary_dh);
        */
        ///fclose(output_det);

        ///file_increment++;

        //      printf("file_increment = %d\n", file_increment);
    }
    stopped=0;
    fclose(log);
    time(&end);
    seconds = difftime(end, now);
    printf("time = %f seconds\n", seconds);
    ///getc(stdin);///uncomment to wait for ENTER key on completion
#ifndef __cplusplus///the Qt version of this function has void return type
    return 0;
#endif
}

vector vec_num (double num, vector vec){
    vector temp;
    temp.x = num*vec.x;
    temp.y = num*vec.y;
    temp.z = num*vec.z;
    return (temp);
}
vector vec_plus (vector vec1, vector vec2){
    vector temp;
    temp.x = vec1.x + vec2.x;
    temp.y = vec1.y + vec2.y;
    temp.z = vec1.z + vec2.z;
    return (temp);
}
vector vec_minus (vector vec1, vector vec2){
    vector temp;
    temp.x = vec1.x - vec2.x;
    temp.y = vec1.y - vec2.y;
    temp.z = vec1.z - vec2.z;
    return (temp);
}
vector vec_cross(vector vec1, vector vec2){
    vector temp;
    temp.x = vec1.y*vec2.z - vec1.z*vec2.y;
    temp.y = vec1.z*vec2.x - vec1.x*vec2.z;
    temp.z = vec1.x*vec2.y - vec1.y*vec2.x;
    return (temp);
}
double vec_dist(vector vec1, vector vec2){
    double dist;
    dist = sqrt(pow(vec1.x - vec2.x, 2.) + pow(vec1.y - vec2.y, 2.) + pow(vec1.z - vec2.z, 2.));
    return (dist);
}
double vec_abs(vector vec){
    double temp;
    temp = sqrt(pow(vec.x, 2.) + pow(vec.y, 2.) + pow(vec.z, 2.));
    return (temp);
}
double vec_dot(vector vec1, vector vec2){
    double temp;
    temp = vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z;
    return (temp);
}
int crystal(vector pos){
    //cube
    switch(g_shape){
    case 0:
        if (cabs(pos.x) <= g_cube_l / 2.0 && cabs(pos.z) <= g_cube_h / 2.0 && cabs(pos.y) <= g_cube_w / 2.0){
            return (1);
        }
        else
        {
            return (0);
        }
        break;
    case 1:break;
    case 2:break;
    case 3:break;
    }
    return 0;


    //sphere
    /*
    if (vec_abs(pos) <= g_sphere_r)
    return (1);
    else
    return (0);
*/
}
vector displacement(vector pos){
    vector u;

    u.x = 0.0;
    u.y = 0.0;
    u.z = 0.0;
    //	u.z = (pos.y + (exp(-c*(cube_w/2.0 + pos.y)) - exp(-c*(cube_w/2.0 - pos.y)))/c)*1e-4;
    //  u.z = 10.085*9.0*1e-4*atan2(pos.y, pos.x)/(2.0*pi);
    //  u.z = 0.0;
    //  u.z = sin(pos.x*2.0)*exp(-(pos.z+cube_h/2.0)/1.0)*1e-4;
    return (u);
}
matrix rot_m(vector vec, double dth){
    double c, s, t;
    matrix R;
    c = cos(dth);
    s = sin(dth);
    t = 1.0 - cos(dth);
    R.e11 = t*vec.x*vec.x + c;
    R.e12 = t*vec.x*vec.y - s*vec.z;
    R.e13 = t*vec.x*vec.z + s*vec.y;
    R.e21 = t*vec.x*vec.y + s*vec.z;
    R.e22 = t*vec.y*vec.y + c;
    R.e23 = t*vec.y*vec.z - s*vec.x;
    R.e31 = t*vec.x*vec.z - s*vec.y;
    R.e32 = t*vec.y*vec.z + s*vec.x;
    R.e33 = t*vec.z*vec.z + c;
    return (R);
}
vector rotation(matrix R, vector vec){
    vector u;
    u.x = R.e11*vec.x + R.e12*vec.y + R.e13*vec.z;
    u.y = R.e21*vec.x + R.e22*vec.y + R.e23*vec.z;
    u.z = R.e31*vec.x + R.e32*vec.y + R.e33*vec.z;
    return (u);
}
vector oblique_to_cartesian(double l1, vector s1, double l2, vector s2, double l3, vector s3){
    vector cartesian;
    cartesian = vec_plus(vec_num(l1, s1), vec_num(l2, s2));
    cartesian = vec_plus(cartesian, vec_num(l3, s3));
    return (cartesian);
}
complex planewave_beam(vector pos){
    complex wave;
    wave = 1.0;
    return (wave);
}
complex gauss_beam (double k, double w0, vector z_axis, vector r0, vector pos){
    vector diff;
    double zp, rp, ws;
    complex f, g;
    diff = vec_minus(pos, r0);
    zp = vec_dot(diff, z_axis);
    rp = sqrt(pow(vec_abs(diff), 2.0) - zp*zp);
    ws = w0*w0*(1.0 + pow(2.0*zp/(k*w0*w0),2.0));
    f = -(cpow(rp,2.0)/ws)*(1.0 - 2.0*I*zp/(k*w0*w0));
    g = 1.0/(1.0 + 2.0*I*zp/(k*w0*w0));
    return(g*cexp(f));
}
complex FZP (double k, double dia_inner, double dia_outer, double f, vector z_axis, vector r0, vector pos){
    vector diff;
    double zp, rp, dr, r;
    complex sum = 0.0;
    int i, n = 1024;
    dr = (dia_outer - dia_inner)/(n-1);
    diff = vec_minus(pos, r0);
    zp = f + vec_dot(diff, z_axis);
    rp = sqrt(pow(vec_abs(diff), 2.0) - pow(vec_dot(diff, z_axis), 2.0));
    for (i = 1; i <= n; i++){
        r = dia_inner + (i - 1)*dr;
        sum = sum + cexp(I*0.5*k*(1.0/zp - 1.0/f)*r*r)*j0(k*rp*r/zp)*r*dr;
    }
    return(-amplitude*sum*I*k*cexp(I*0.5*k*rp*rp/zp)/zp);
}
