//main header file; contains various constants, structs, and macros; all configurable parameters should be defined at the end of this file
#ifndef MAIN_H
#define MAIN_H

#include"matheval.h"//needed for function parameters
#include"hdf5.h"//standard hdf5 library
#include"hdf5_hl.h"//hdf5 lite library

extern hid_t hdf5file;

static const double pi=3.1415926535897932384626433832795;
typedef struct{double x,y,z;}vector;
typedef struct{double e11,e12,e13,e21,e22,e23,e31,e32,e33;}matrix;
typedef struct{int back,front,num;}boundary;
typedef struct{
    double pix_size; //pixel size in um
    int row; //number of pixels in horizontal direction
    int column; //number of pixels in horizontal direction
    double dist; //distance to origin
}detector;

#ifndef __cplusplus//if not using C++...
#include<complex.h>//use C99 complex numbers
#else//otherwise...
#include<complex>//use C++ complex numbers
static const std::complex<double>I(0,1);//define the imaginary unit
#define complex std::complex<double>//change C99 to C++ syntax as necessary
#define cabs abs
#define cexp exp
#define cimag std::imag
#define creal std::real
#define csqrt sqrt
#define cpow pow

#include<QThread>
class Main:public QThread{
    Q_OBJECT
    volatile bool stopped=false;
    volatile int steps=0;
public:
    void run();
signals:
    void starting();
    void output(QString);
    void progress(int);
    void max(int);
public slots:
    void begin();
    void stop();
};
static int flagset(std::initializer_list<int>list){//used for linking Params with the LNK macro
    int flags=0;
    for(int i:list)flags|=1<<i;
    return flags;
}
#endif

vector vec_num(double, vector);
vector vec_plus(vector, vector);
vector vec_minus(vector, vector);
double vec_dist(vector, vector);
double vec_abs(vector);
double vec_dot(vector, vector);
vector vec_cross(vector, vector);
vector rotation(matrix, vector);
matrix rot_m(vector, double);

int crystal(vector);
vector oblique_to_cartesian(double, vector, double, vector, double, vector);

vector displacement(vector);
complex planewave_beam(vector);
complex gauss_beam(double, double, vector, vector, vector);
complex FZP(double, double, double, double, vector, vector, vector);

//below defines the NARGS macro, used for counting an arbitrary number of arguments up to 9
//not currently used, but may be potentially useful as more macros are added
#define NARGS(...) (_NARGS(__VA_ARGS__,9,8,7,6,5,4,3,2,1,0)-(sizeof#__VA_ARGS__==1))
#define _NARGS(_1,_2,_3,_4,_5,_6,_7,_8,_9,N,...) N
#endif//do all of the above only once per file

//block of 'magic' macros to simplify gui and parameter initialization; 'DEF mode' used in "main.cpp" & 'GUI mode' used in "window.cpp"
/*
#ifdef GUI
#undef GUI
#define CODE(...) __VA_ARGS__
#define PNL(name) box=new QGroupBox(name,frame);middle=new QHBoxLayout(box);middle->setContentsMargins(0,0,0,0);widget=new QWidget(box);box->setStyleSheet("font-weight:bold");widget->setStyleSheet("font-weight:normal");middle->addWidget(widget);inner=new QVBoxLayout(widget);outer->addWidget(box);box->setCheckable(true);connect(box,QGroupBox::toggled,widget,QWidget::setVisible);
#define CMN inner->addWidget(param);connect(thread,Main::starting,param,Param::upd8);params[index++]=param;
#define MNU(name,type,var,...) param=combo=new ComboParam(name,&var,{__VA_ARGS__},box);CMN
#define PRM(name,type,var,...) param=newParam(name,&var,box);CMN
#define LNK(...) param->link(flagset({__VA_ARGS__}));param->link(0);connect(combo,ComboParam::changed,param,Param::link);
#else
*/
#ifdef USE_ALL_MACROS
#define CODE(...) __VA_ARGS__
#else//if USE_ALL_MACROS is not defined, process only macros used for declaring parameters
#define CODE(...)
#define LNK(...)
#define MUL(...)
#define GRP(...)
#define MNU(name,var,...) PRM(name,int,var,0)
#endif
#ifdef FOREACH//if FOREACH is defined, apply it to each parameter
#define PRM FOREACH
#else//otherwise, declare each of the parameters
#define PRM(name,type,var,...) extern type var;
#endif
//#endif

//declare all configurable global parameters below using the PRM(parameter) and MNU(menu) macros
//the variable given to a MNU is set to the index of the currently selected item, beginning at zero
//follow a PRM with LNK(link) to link the parameter to the given values of the last added MNU, may be combined with MUL
//follow a PRM with MUL(multiply) to multiply the user input by a conversion value before giving it to the computation (doubles only)
//use the GRP(group) macro to create a new group of parameters
//the CODE(code) macro is used by the GUI implementation to insert irregular widgets into the parameter panels
//whitespace may be added anywhere between macros and macro arguments for readability

//GRP syntax: GRP("display name of parameter group")
//MNU syntax: MNU("variable display name",variable type,variable name,"display names","of each","menu option")
//PRM syntax: PRM("variable display name",variable type,variable name,variable default value)
//LNK syntax: PRM(...)LNK(index0,index1,index2)
//MUL syntax: PRM(...)MUL(conversion constant)

GRP("Crystal")
MNU("Shape",g_shape,"Rectangle","Cylinder","Sphere","Hexagon")//Rect=0,Cyl=1,Sphr=2,Hex=3
PRM("Rectangle length(μm)",double,g_cube_l,.15)LNK(0)MUL(1)
PRM("Rectangle width(μm)",double,g_cube_w,.15)LNK(0)MUL(1)
PRM("Rectangle height(μm)",double,g_cube_h,.15)LNK(0)MUL(1)
PRM("Sphere radius(μm)",double,g_sphere_r,.3)LNK(2)MUL(1)
PRM("d spacing(Å)",double,g_d_spacing,5.43/8.0)MUL(1e-4)
CODE(array[0]=param;param->attach(new DialogButton("Calculate",spacing,[=]{((DoubleParam*)param)->set(spacing->result());}));)
PRM("Direction of h vector",vector,g_unit_h,{0,0,-1})CODE(array[3]=param;)
PRM("X0",complex,chi_0,-0.74984e-5+I*0.88508e-7)
PRM("Xh",complex,chi_h,-0.17751e-5+I*0.66803e-7)
PRM("X displacement function",void*,g_xeval,evaluator_create("x"))
PRM("Y displacement function",void*,g_yeval,evaluator_create("y"))
PRM("Z displacement function",void*,g_zeval,evaluator_create("z"))

GRP("Beam")
PRM("Energy(keV)",double,g_energy,11.4)CODE(array[1]=param;)
PRM("Bragg angle(°)",double,g_th_Bragg,180)CODE(array[2]=param;)MUL(pi/180)
PRM("Incident beam direction",vector,g_unit_s0,{NAN,NAN,NAN})
CODE(direction->setParams((DoubleParam*)array[0],(DoubleParam*)array[1],(DoubleParam*)array[2],(VectorParam*)array[3],(VectorParam*)param);param->attach(new DialogButton("Calculate",direction,[=]{((VectorParam*)param)->set(direction->result());}));)
MNU("Beam type",g_beam_type,"Plane Wave","Gaussian","Fresnel Zone Plate")//PW=0,G=1,FZP=2
PRM("Flux density(γ/mm²s)",double,g_flux_density,NAN)LNK(0,2)MUL(1e-6)
PRM("Efficiency",double,g_efficiency,NAN)LNK(2)
PRM("Inner radius(μm)",double,g_inner_radius,NAN)LNK(2)MUL(1)
PRM("Outer radius(μm)",double,g_outer_radius,NAN)LNK(2)MUL(1)
PRM("Focal length(μm)",double,g_focal_length,147000)LNK(2)MUL(1)
PRM("r0 vector",vector,g_r0,{NAN,NAN,NAN})LNK(1,2)
PRM("Width(μm)",double,g_w0,NAN)LNK(1)MUL(1)
PRM("Total flux(γ/s)",double,g_total_flux,NAN)LNK(1)MUL(1)

GRP("Detector")
PRM("Pixel size(μm)",double,g_pixel_size,55)
PRM("Number of rows",int,g_row,256)
PRM("Number of columns",int,g_column,256)
PRM("Sample to detector distance(m)",double,g_det_dist,1.5)MUL(1e6)

GRP("Computation")
PRM("Grid cell size(μm)",double,g_ds,0.005)MUL(1)
PRM("Slice step size(μm)",double,g_dy,0.005)MUL(1)
PRM("Accuracy of the solution",double,g_tolerance,1e-6)
PRM("Start deviation angle(°)",double,g_dth_start,0.0e-4)MUL(pi/180)
PRM("Angular step(°)",double,g_dth_step,1e-6)MUL(pi/180)
PRM("Number of angular scan points",int,g_num_angle,1)
PRM("Number of spiral scan points",int,g_num_scan,1)
PRM("Spiral scan radius(μm)",double,g_spiral_c,0.1)MUL(1)

#undef GRP
#undef LNK
#undef MNU
#undef MUL
#undef PRM
#undef CODE
#undef FOREACH
#undef USE_ALL_MACROS
