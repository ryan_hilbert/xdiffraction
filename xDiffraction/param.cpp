#include"param.h"
#include"matheval.h"

Param::Param(const char*name,void*dfault,QWidget*parent):QWidget(parent),label(new QLabel(name)),data(dfault),layout(new QHBoxLayout(this)){
    layout->setMargin(0);
    layout->addWidget(label);
}
QString Param::text(){
    return label->text();
}
void Param::link(int flags){
    if(links)setVisible(links&(1<<flags));
    else links=flags;
}
void Param::attach(QWidget*widget){
    layout->addWidget(widget);
}

IntParam::IntParam(const char*name,int*dfault,QWidget*parent):Param(name,dfault,parent),edit(new QLineEdit(QString::number(*dfault),this)){
    edit->setValidator(new QIntValidator());
    layout->addWidget(edit);
    connect(edit,QLineEdit::editingFinished,this,Param::changed);
}
void IntParam::upd8(){
    *(int*)data=edit->text().toInt();
}
void IntParam::display(){
    edit->setText(QString::number(*(int*)data));
}
void IntParam::clear(){
    edit->setText("");
}
herr_t IntParam::hdf5read(hid_t hid){
    int input;
    herr_t err=H5LTget_attribute_int(hid,".",text().toUtf8().data(),&input);
    edit->setText(QString::number(input));
    return err;
}
herr_t IntParam::hdf5write(hid_t hid){
    const int output=edit->text().toInt();
    return H5LTset_attribute_int(hid,".",text().toUtf8().data(),&output,1);
}

DoubleParam::DoubleParam(const char*name,double*dfault,QWidget*parent):Param(name,dfault,parent),edit(new QLineEdit(QString::number(*dfault),this)){
    edit->setValidator(new QDoubleValidator());
    layout->addWidget(edit);
    connect(edit,QLineEdit::editingFinished,this,Param::changed);
}
void DoubleParam::upd8(){
    *(double*)data=edit->text().toDouble()*multiplier;
}
void DoubleParam::display(){
    edit->setText(QString::number(*(double*)data/multiplier));
}
void DoubleParam::clear(){
    edit->setText("");
}
void DoubleParam::set(QString str){
    edit->setText(str);
}
double DoubleParam::setMultiplier(double mul){
    double temp=multiplier;
    multiplier=mul;
    return temp;
}
QString DoubleParam::get(){
    return edit->text();
}
herr_t DoubleParam::hdf5read(hid_t hid){
    double input;
    herr_t err=H5LTget_attribute_double(hid,".",text().toUtf8().data(),&input);
    edit->setText(QString::number(input));
    return err;
}
herr_t DoubleParam::hdf5write(hid_t hid){
    const double output=edit->text().toDouble();
    return H5LTset_attribute_double(hid,".",text().toUtf8().data(),&output,1);
}

ComplexParam::ComplexParam(const char*name,complex*dfault,QWidget*parent):Param(name,dfault,parent),edit(new QLineEdit(QString::number(dfault->real()),this)),editI(new QLineEdit(QString::number(dfault->imag()),this)){
    edit->setValidator(new QDoubleValidator());
    editI->setValidator(new QDoubleValidator());
    layout->addWidget(edit);
    layout->addWidget(new QLabel("+"));
    layout->addWidget(editI);
    layout->addWidget(new QLabel("i"));
    connect(edit,QLineEdit::editingFinished,this,Param::changed);
    connect(editI,QLineEdit::editingFinished,this,Param::changed);
}
void ComplexParam::upd8(){
    *(complex*)data=complex(edit->text().toDouble(),editI->text().toDouble());
}
void ComplexParam::display(){
    edit->setText(QString::number(creal(*(complex*)data)));
    editI->setText(QString::number(cimag(*(complex*)data)));
}
void ComplexParam::clear(){
    edit->setText("");
    editI->setText("");
}
herr_t ComplexParam::hdf5read(hid_t hid){
    double r,i;
    herr_t err=H5LTget_attribute_double(hid,".",text().append(" real").toUtf8().data(),&r)
    |H5LTget_attribute_double(hid,".",text().append(" imag").toUtf8().data(),&i);
    edit->setText(QString::number(r));
    editI->setText(QString::number(i));
    return err;
}
herr_t ComplexParam::hdf5write(hid_t hid){
    const double r=edit->text().toDouble(),i=editI->text().toDouble();
    return H5LTset_attribute_double(hid,".",text().append(" real").toUtf8().data(),&r,1)
    |H5LTset_attribute_double(hid,".",text().append(+" imag").toUtf8().data(),&i,1);
}

VectorParam::VectorParam(const char*name,vector*dfault,QWidget*parent):Param(name,dfault,parent),editX(new QLineEdit(QString::number(dfault->x),this)),editY(new QLineEdit(QString::number(dfault->y),this)),editZ(new QLineEdit(QString::number(dfault->z),this)){
    editX->setValidator(new QDoubleValidator(editX));
    editY->setValidator(new QDoubleValidator(editY));
    editZ->setValidator(new QDoubleValidator(editZ));
    layout->addWidget(editX);
    layout->addWidget(new QLabel("x"));
    layout->addWidget(editY);
    layout->addWidget(new QLabel("y"));
    layout->addWidget(editZ);
    layout->addWidget(new QLabel("z"));
    connect(editX,QLineEdit::editingFinished,this,Param::changed);
    connect(editY,QLineEdit::editingFinished,this,Param::changed);
    connect(editZ,QLineEdit::editingFinished,this,Param::changed);
}
void VectorParam::upd8(){
    *(vector*)data={editX->text().toDouble(),editY->text().toDouble(),editZ->text().toDouble()};
}
void VectorParam::display(){
    editX->setText(QString::number(((vector*)data)->x));
    editY->setText(QString::number(((vector*)data)->y));
    editZ->setText(QString::number(((vector*)data)->z));
}
void VectorParam::clear(){
    editX->setText("");
    editY->setText("");
    editZ->setText("");
}
void VectorParam::set(vector v){
    editX->setText(QString::number(v.x));
    editY->setText(QString::number(v.y));
    editZ->setText(QString::number(v.z));
}
vector VectorParam::get(){
    return{editX->text().toDouble(),editY->text().toDouble(),editZ->text().toDouble()};
}
herr_t VectorParam::hdf5read(hid_t hid){
    double x,y,z;
    herr_t err=H5LTget_attribute_double(hid,".",text().append(" x").toUtf8().data(),&x)
    |H5LTget_attribute_double(hid,".",text().append(" y").toUtf8().data(),&y)
    |H5LTget_attribute_double(hid,".",text().append(" z").toUtf8().data(),&z);
    editX->setText(QString::number(x));
    editY->setText(QString::number(y));
    editZ->setText(QString::number(z));
    return err;
}
herr_t VectorParam::hdf5write(hid_t hid){
    const double x=editX->text().toDouble(),y=editY->text().toDouble(),z=editZ->text().toDouble();
    return H5LTset_attribute_double(hid,".",text().append(" x").toUtf8().data(),&x,1)
    |H5LTset_attribute_double(hid,".",text().append(" y").toUtf8().data(),&y,1)
    |H5LTset_attribute_double(hid,".",text().append(" z").toUtf8().data(),&z,1);
}

FunctionParam::FunctionParam(const char*name,void**dfault,QWidget*parent):Param(name,dfault,parent),edit(new QLineEdit(evaluator_get_string(*dfault),this)){
    layout->addWidget(edit);
    connect(edit,QLineEdit::editingFinished,this,Param::changed);
}
void FunctionParam::upd8(){
    void*evaluator=evaluator_create(edit->text().toUtf8().data());
    if(evaluator){
        evaluator_destroy(*(void**)data);
        *(void**)data=evaluator;
    }
    else edit->setText(edit->text()+" is invalid; instead using "+evaluator_get_string(*(void**)data));
}
void FunctionParam::display(){
    edit->setText(evaluator_get_string(*(void**)data));
}
void FunctionParam::clear(){
    edit->setText("");
}
herr_t FunctionParam::hdf5read(hid_t hid){
    char input[BUFSIZ];
    herr_t err=H5LTget_attribute_string(hid,".",text().toUtf8().data(),input);
    edit->setText(input);
    return err;
}
herr_t FunctionParam::hdf5write(hid_t hid){
    return H5LTset_attribute_string(hid,".",text().toUtf8().data(),edit->text().toUtf8().data());
}

ComboParam::ComboParam(const char*name,int*dfault,std::initializer_list<QString>list,QWidget*parent):Param(name,dfault,parent),box(new QComboBox(this)){
    box->insertItems(0,list);
    layout->addWidget(box);
    connect(box,SIGNAL(currentIndexChanged(int)),this,SIGNAL(currentIndexChanged(int)));
}
void ComboParam::upd8(){
    *(int*)data=box->currentIndex();
}
void ComboParam::display(){
    box->setCurrentIndex(*(int*)data);
}
void ComboParam::clear(){
    box->setCurrentIndex(-1);
}
herr_t ComboParam::hdf5read(hid_t hid){
    char input[BUFSIZ];
    herr_t err=H5LTget_attribute_string(hid,".",text().toUtf8().data(),input);
    box->setCurrentIndex(box->findText(input));
    return err;
}
herr_t ComboParam::hdf5write(hid_t hid){
    return H5LTset_attribute_string(hid,".",text().toUtf8().data(),box->currentText().toUtf8().data());
}

IntParam*newParam(const char*name,int*dfault,QWidget*parent){return new IntParam(name,dfault,parent);}
DoubleParam*newParam(const char*name,double*dfault,QWidget*parent){return new DoubleParam(name,dfault,parent);}
ComplexParam*newParam(const char*name,complex*dfault,QWidget*parent){return new ComplexParam(name,dfault,parent);}
VectorParam*newParam(const char*name,vector*dfault,QWidget*parent){return new VectorParam(name,dfault,parent);}
FunctionParam*newParam(const char*name,void**dfault,QWidget*parent){return new FunctionParam(name,dfault,parent);}
