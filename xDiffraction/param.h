#ifndef PARAM_H
#define PARAM_H

#include<QtWidgets>
#include<initializer_list>
#include"main.h"

//These classes are responsible for displaying a configurable parameter to the user
//Param itself cannot be instantiated and must be subclassed for each type of parameter

class Param:public QWidget{
    Q_OBJECT
    QLabel*label;
    int links=0;//set of flags for linking Params to ComboParams
protected:
    void*data;//The piece of data to be updated by the interface in the upd8 slot
    QHBoxLayout*layout;//The layout that all components should be added to
public:
    Param(const char*,void*,QWidget*parent=0);
    QString text();
signals:
    void changed();
public slots:
    void link(int);
    void attach(QWidget*);
    virtual void upd8()=0;
    virtual void display()=0;
    virtual void clear()=0;
    virtual herr_t hdf5read(hid_t)=0;
    virtual herr_t hdf5write(hid_t)=0;
};

class IntParam:public Param{
    QLineEdit*edit;
public:
    IntParam(const char*,int*,QWidget*parent=0);
    void upd8();
    void display();
    void clear();
    herr_t hdf5read(hid_t);
    herr_t hdf5write(hid_t);
};

class DoubleParam:public Param{
    QLineEdit*edit;
    double multiplier=1;//number to multiply by to convert input to desired units
public:
    DoubleParam(const char*,double*,QWidget*parent=0);
    void upd8();
    void display();
    void clear();
    void set(QString);
    double setMultiplier(double);
    QString get();
    herr_t hdf5read(hid_t);
    herr_t hdf5write(hid_t);
};

class ComplexParam:public Param{
    QLineEdit*edit,*editI;
public:
    ComplexParam(const char*,complex*,QWidget*parent=0);
    void upd8();
    void display();
    void clear();
    herr_t hdf5read(hid_t);
    herr_t hdf5write(hid_t);
};

class VectorParam:public Param{
    QLineEdit*editX,*editY,*editZ;
public:
    VectorParam(const char*,vector*,QWidget*parent=0);
    void upd8();
    void display();
    void clear();
    void set(vector);
    vector get();
    herr_t hdf5read(hid_t);
    herr_t hdf5write(hid_t);
};

class FunctionParam:public Param{
    QLineEdit*edit;
public:
    FunctionParam(const char*,void**,QWidget*parent=0);
    void upd8();
    void display();
    void clear();
    herr_t hdf5read(hid_t);
    herr_t hdf5write(hid_t);
};

class ComboParam:public Param{
    Q_OBJECT
    QComboBox*box;
public:
    ComboParam(const char*,int*,std::initializer_list<QString>,QWidget*parent=0);
    void upd8();
    void display();
    void clear();
    herr_t hdf5read(hid_t);
    herr_t hdf5write(hid_t);
signals:
    void currentIndexChanged(int);
};

//Overloaded convenience methods for generating Param widgets
IntParam*newParam(const char*,int*,QWidget*parent=0);
DoubleParam*newParam(const char*,double*,QWidget*parent=0);
ComplexParam*newParam(const char*,complex*,QWidget*parent=0);
VectorParam*newParam(const char*,vector*,QWidget*parent=0);
FunctionParam*newParam(const char*,void**,QWidget*parent=0);
#endif
