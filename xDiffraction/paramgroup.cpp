#include "paramgroup.h"
ParamGroup::ParamGroup(QString name,QWidget*parent):QGroupBox(name,parent){
    QLayout*outer=new QVBoxLayout(this);
    outer->setContentsMargins(0,0,0,0);
    QWidget*frame=new QWidget(this);
    outer->addWidget(frame);
    frame->setLayout(layout);

    setStyleSheet("font-weight:bold");
    frame->setStyleSheet("font-weight:normal");
    setCheckable(true);
    connect(this,QGroupBox::toggled,frame,QWidget::setVisible);
}
void ParamGroup::addParam(Param*param){
    layout->addWidget(param);
    push_back(param);
}
void ParamGroup::enable(){
    for(auto param:*this)param->setEnabled(true);
}
void ParamGroup::disable(){
    for(auto param:*this)param->setEnabled(false);
}
