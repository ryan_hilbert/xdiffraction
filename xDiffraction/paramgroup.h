#ifndef PARAMGROUP_H
#define PARAMGROUP_H
#include<QtWidgets>
#include"param.h"
class ParamGroup:public QGroupBox,private std::vector<Param*>{
    QLayout*layout=new QVBoxLayout();
public:
    ParamGroup(QString name="",QWidget*parent=0);
    using std::vector<Param*>::begin;
    using std::vector<Param*>::end;
    void addParam(Param*);
public slots:
    void enable();
    void disable();
};
#endif
