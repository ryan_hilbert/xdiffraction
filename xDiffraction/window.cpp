#include<QtWidgets>

#include"window.h"
#include"paramgroup.h"
#include"main.h"
#include"dialogbutton.h"
#include"directioncalculator.h"
#include"spacingcalculator.h"

Window::Window(QWidget*parent):QMainWindow(parent){
    setWindowTitle("Unsaved Parameter List");
    QMenu*menu=menuBar()->addMenu("File");
    menu->addAction("New",this,SLOT(clear()),QKeySequence::New);
    menu->addAction("Load...",this,SLOT(load()),QKeySequence::Open);
    menu->addAction("Save",this,SLOT(save()),QKeySequence::Save);
    menu->addAction("Save As...",this,SLOT(saveAs()),QKeySequence::SaveAs);

    QWidget*base=new QWidget(this);
    QHBoxLayout*hlayout=new QHBoxLayout(base);
    setCentralWidget(base);
    QScrollArea*scroll=new QScrollArea(base);
    scroll->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Preferred);
    hlayout->addWidget(scroll);
    QWidget*frame=new QWidget(scroll);
    QVBoxLayout*groupLayout=new QVBoxLayout(frame);
    ParamGroup*group;
    Param*param;
    ComboParam*combo;
    Param*array[4]={0};
    SpacingCalculator*spacing=new SpacingCalculator();
    DirectionCalculator*direction=new DirectionCalculator();
#define USE_ALL_MACROS
#define GRP(...) group=new ParamGroup(__VA_ARGS__);groupLayout->addWidget(group);push_back(group);
#define CMN group->addParam(param);connect(thread,Main::starting,param,Param::upd8);
#define MNU(name,var,...) param=combo=new ComboParam(name,&var,{__VA_ARGS__});CMN
#define FOREACH(name,type,var,...) param=newParam(name,&var);CMN
#define LNK(...) param->link(flagset({__VA_ARGS__}));param->link(0);connect(combo,ComboParam::currentIndexChanged,param,Param::link);
#define MUL(...) ((DoubleParam*)param)->setMultiplier(__VA_ARGS__);
#include"main.h"
#undef CMN
    groupLayout->addSpacerItem(new QSpacerItem(0,0,QSizePolicy::Fixed,QSizePolicy::Expanding));
    scroll->setWidgetResizable(true);
    scroll->setWidget(frame);
    QWidget*vertical=new QWidget(base);
    hlayout->addWidget(vertical);
    QVBoxLayout*vlayout=new QVBoxLayout(vertical);
    vlayout->setContentsMargins(0,0,0,0);
    vlayout->addWidget(detectrogram);
    connect(thread,Main::progress,detectrogram,Detectrogram::max);

    QHBoxLayout*blayout=new QHBoxLayout;
    vlayout->addLayout(blayout);
    QPushButton*begin=new QPushButton("Start Calculation");
    connect(begin,QPushButton::clicked,thread,Main::begin);
    connect(thread,Main::starting,this,Window::lock);
    connect(thread,Main::starting,[=]{begin->setEnabled(false);});
    connect(thread,Main::finished,[=]{begin->setEnabled(true);});
    blayout->addWidget(begin);

    QPushButton*halt=new QPushButton("Halt Calculation");
    connect(halt,QPushButton::clicked,thread,Main::stop);
    connect(halt,QPushButton::clicked,[=]{halt->setEnabled(false);});
    connect(thread,Main::starting,[=]{halt->setEnabled(true);});
    connect(thread,Main::finished,[=]{halt->setEnabled(false);});
    halt->setEnabled(false);
    blayout->addWidget(halt);

    QPlainTextEdit*console=new QPlainTextEdit("Console initialized!\n");
    QPalette palette=console->palette();
    palette.setColor(QPalette::Base,Qt::black);
    palette.setColor(QPalette::Text,Qt::white);
    console->setPalette(palette);
    console->setFont(QFont("Courier"));
    console->setReadOnly(true);
    connect(thread,Main::output,console,QPlainTextEdit::appendPlainText);
    vlayout->addWidget(console);

    QProgressBar*bar=new QProgressBar;
    bar->setStyleSheet("");
    bar->setValue(0);
    bar->setTextVisible(true);
    connect(thread,Main::max,bar,QProgressBar::setMaximum);
    connect(thread,Main::progress,bar,QProgressBar::setValue);
    connect(thread,Main::starting,[=]{bar->setEnabled(true);bar->setStyleSheet("text-align:center");});
    connect(thread,QThread::finished,[=]{bar->setEnabled(false);bar->setStyleSheet("QProgressBar{text-align:center;}QProgressBar::chunk{background-color:lightgray;}");});
    vlayout->addWidget(bar);

    FILE*f=fopen("~.h5","r");
    if(f){
        hid_t hid=H5Fopen("~.h5",H5F_ACC_RDONLY,H5P_DEFAULT);
        for(auto group:*this)for(auto param:*group)param->hdf5read(hid);
        H5Fclose(hid);
        fclose(f);
    }
}
Window::~Window(){
    if(hdf5file>0)H5Fclose(hdf5file);
    hdf5file=H5Fcreate("~.h5",H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
    if(hdf5file>0){
        for(auto group:*this)for(auto param:*group)param->hdf5write(hdf5file);
        H5Fclose(hdf5file);
    }
}
void Window::clear(){
    if(QMessageBox::Ok==QMessageBox::warning(this,"Confirm","This will clear your current parameters. Are you sure?",QMessageBox::Ok|QMessageBox::Cancel,QMessageBox::Cancel)){
        if(hdf5file>0)H5Fclose(hdf5file);
        for(auto group:*this)for(auto param:*group)param->clear();
        setWindowTitle("Unsaved Parameter List");
        unlock();
    }
}
void Window::load(){
    QString name=QFileDialog::getOpenFileName(this,"Open","","HDF5 (*.h5)");
    if(!name.isEmpty()){
        if(hdf5file>0)H5Fclose(hdf5file);
        hdf5file=H5Fopen(name.toUtf8().data(),H5F_ACC_RDWR,H5P_DEFAULT);
        for(auto group:*this){
            hid_t hid=H5Gopen1(hdf5file,group->title().toUtf8().data());
            for(auto param:*group)param->hdf5read(hid);
            H5Gclose(hid);
        }
        setWindowTitle(QFileInfo(name).fileName());
        hid_t hid=H5Gopen1(hdf5file,"/Detector");
        if(H5LTfind_attribute(hid,"Images Completed")){
            detectrogram->load();
            thread->stop();
            thread->begin();
            //calling stop() before begin() signals the thread to emit initialization signals without performing any calculations
        }
        H5Gclose(hid);
    }
}
void Window::save(){
    if(hdf5file>0){
        for(auto group:*this){
            hid_t hid=H5Gcreate1(hdf5file,("/"+group->title()).toUtf8().data(),0);
            for(auto param:*group)param->hdf5write(hid);
            H5Gclose(hid);
        }
    }
    else saveAs();
}
void Window::saveAs(){
    QString name=QFileDialog::getSaveFileName(this,"Save As","","HDF5 (*.h5)");
    if(!name.isEmpty()){
        if(hdf5file>0)H5Fclose(hdf5file);
        hdf5file=H5Fcreate(name.toUtf8().data(),H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
        save();
        setWindowTitle(QFileInfo(name).fileName());
    }
}
void Window::lock(){
    for(auto group:*this)group->disable();
}
void Window::unlock(){
    for(auto group:*this)group->enable();
}
