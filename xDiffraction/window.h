#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include<QMainWindow>
#include"main.h"
#include"param.h"
#include"paramgroup.h"
#include"detectrogram.h"
class Window:public QMainWindow,private std::vector<ParamGroup*>{
    Q_OBJECT
    Main*thread=new Main;
    Detectrogram*detectrogram=new Detectrogram(this);
public:
    Window(QWidget*parent=0);
    ~Window();
    using std::vector<ParamGroup*>::begin;
    using std::vector<ParamGroup*>::end;
public slots:
    void clear();
    void load();
    void save();
    void saveAs();
    void lock();
    void unlock();
};
#endif
