#-------------------------------------------------
#
# Project created by QtCreator 2015-01-26T01:13:06
#
#-------------------------------------------------

QT += core gui printsupport widgets
TARGET = xDiffraction
TEMPLATE = app

SOURCES += param.cpp window.cpp dialogbutton.cpp dialog.cpp spacingcalculator.cpp directioncalculator.cpp main.cpp paramgroup.cpp detectrogram.cpp
HEADERS += param.h window.h main.h dialogbutton.h dialog.h spacingcalculator.h directioncalculator.h paramgroup.h detectrogram.h

SOURCES += error.c g77_interface.c matheval.c node.c parser.c scanner.c symbol_table.c xmalloc.c xmath.c
HEADERS += common.h error.h matheval.h node.h parser.h symbol_table.h xmalloc.h xmath.h

SOURCES += qcustomplot.cpp
HEADERS += qcustomplot.h

QMAKE_CXXFLAGS += -std=c++11 -fopenmp

LIBS += -lhdf5 -lhdf5_hl -fopenmp
